package com.ricardo.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ricardo.gruposalinas.app.entity.Serpiente;

@Repository
public interface SerpienteRepositorio extends JpaRepository<Serpiente, Long> {

}
