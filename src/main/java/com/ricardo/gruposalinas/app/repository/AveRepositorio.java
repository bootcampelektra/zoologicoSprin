package com.ricardo.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ricardo.gruposalinas.app.entity.Ave;

@Repository
public interface AveRepositorio extends JpaRepository<Ave, Long> {

}
