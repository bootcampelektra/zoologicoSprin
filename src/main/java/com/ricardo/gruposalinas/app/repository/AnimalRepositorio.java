package com.ricardo.gruposalinas.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ricardo.gruposalinas.app.entity.Animal;

@Repository
public interface AnimalRepositorio extends JpaRepository<Animal, Long>{

}
