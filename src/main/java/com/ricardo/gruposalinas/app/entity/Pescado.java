package com.ricardo.gruposalinas.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "pescados")
public class Pescado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column (length = 50)
	private String Cola;
	private Long aletas;
	
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCola() {
		return Cola;
	}
	public void setCola(String cola) {
		Cola = cola;
	}
	public Long getAletas() {
		return aletas;
	}
	public void setAletas(Long aletas) {
		this.aletas = aletas;
	}
	
	
}
