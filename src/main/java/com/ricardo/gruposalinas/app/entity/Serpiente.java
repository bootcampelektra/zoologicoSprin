package com.ricardo.gruposalinas.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "serpiente")
public class Serpiente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@Column (length = 50)
	private String pupilas;
	private Boolean venenosa;
	
	
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPupilas() {
		return pupilas;
	}
	public void setPupilas(String pupilas) {
		this.pupilas = pupilas;
	}
	public Boolean getVenenosa() {
		return venenosa;
	}
	public void setVenenosa(Boolean venenosa) {
		this.venenosa = venenosa;
	}
	
	
}
