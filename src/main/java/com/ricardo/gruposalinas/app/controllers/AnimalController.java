package com.ricardo.gruposalinas.app.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ricardo.gruposalinas.app.entity.Animal;
import com.ricardo.gruposalinas.app.services.AnimalServicio;

@RestController
@RequestMapping ("/api/animales")
public class AnimalController {

	Long datos = (long) 0;
	
	@Autowired
	@Lazy
	private AnimalServicio animalServicio;
	
	
	@PostMapping
	public ResponseEntity<?> create (@RequestBody Animal animal){
		return ResponseEntity.status(HttpStatus.CREATED).body(animalServicio.save(animal));
	}
	
	
	
	@GetMapping("/{id}")
	public  ResponseEntity<?> read(@PathVariable(value = "id") Long id){
		Optional<Animal> oAnimal = animalServicio.findById(id);
		
		if(!oAnimal.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oAnimal);
	}
	
	

	@GetMapping
	public ResponseEntity<?> readAll(){
		Iterable<Animal> oAnimal = animalServicio.findAll();
		
		return ResponseEntity.ok(oAnimal);
	}
	
	
	

	@GetMapping("/Todos")
	public ResponseEntity<?> readAllN(){
		Iterable<Animal> oAnimal = animalServicio.findAll();
		datos = (long) 0;

        oAnimal.forEach(new Consumer<Animal>() {
  
            @Override
            public void accept(Animal t)
            {
  
            	datos++;
            	System.out.println(datos);
            }
  
        });
    
		
		return ResponseEntity.ok("La cantidad es: "+ datos);
	}
	
	
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Animal animalDetalles, @PathVariable(value = "id")Long id){
		Optional<Animal> oAnimal = animalServicio.findById(id);
		
		if(!oAnimal.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oAnimal.get().setNombre(animalDetalles.getNombre());
		oAnimal.get().setEdad(animalDetalles.getEdad());
		oAnimal.get().setMovilidad(animalDetalles.getMovilidad());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(animalServicio.save(oAnimal.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id")Long id){
		Optional<Animal> oAnimal = animalServicio.findById(id);
		
		if(!oAnimal.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		animalServicio.deleteById(oAnimal.get().getId());
		return ResponseEntity.ok(oAnimal);	
	}
	
}
