package com.ricardo.gruposalinas.app.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.ricardo.gruposalinas.app.entity.Pescado;
import com.ricardo.gruposalinas.app.services.PescadoServicio;

@RestController
@RequestMapping ("/api/pescados")
public class PescadoController {
	Long datos = (long) 0;

	@Autowired
	private PescadoServicio pescadoServicio;
	
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Pescado pescado){
		return ResponseEntity.status(HttpStatus.CREATED).body(pescadoServicio.save(pescado));
	}
	
	
	@GetMapping("/{id}")
	public  ResponseEntity<?> read(@PathVariable(value = "id") Long id){
		Optional<Pescado> oPescado = pescadoServicio.findById(id);
		
		if(!oPescado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oPescado);
	}
	
	
	@GetMapping("/Todos")
	public ResponseEntity<?> readAllN(){
		Iterable<Pescado> oFelino = pescadoServicio.findAll();
		datos = (long) 0;

        oFelino.forEach(new Consumer<Pescado>() {
  
            @Override
            public void accept(Pescado t)
            {
  
            	datos++;
            	System.out.println(datos);
            }
  
        });
    
        return ResponseEntity.ok("La cantidad es: "+ datos);
	}
	

	@GetMapping
	public ResponseEntity<?> readAll(){
		Iterable<Pescado> oPescado = pescadoServicio.findAll();
		return ResponseEntity.ok(oPescado);
	}
	
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Pescado pescadoDetalles, @PathVariable(value = "id")Long id){
		Optional<Pescado> oPescado = pescadoServicio.findById(id);
		
		if(!oPescado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oPescado.get().setCola(pescadoDetalles.getCola());
		oPescado.get().setAletas(pescadoDetalles.getAletas());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(pescadoServicio.save(oPescado.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id")Long id){
		Optional<Pescado> oPescado = pescadoServicio.findById(id);
		
		if(!oPescado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		pescadoServicio.deleteById(oPescado.get().getId());
		return ResponseEntity.ok(oPescado);	
	}
	
	
	
}
