package com.ricardo.gruposalinas.app.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ricardo.gruposalinas.app.entity.Ave;
import com.ricardo.gruposalinas.app.services.AveServicio;

@RestController
@RequestMapping ("/api/aves")
public class AveController {
	Long datos = (long) 0;

	@Autowired
	private AveServicio aveServicio;
	
	@PostMapping
	public ResponseEntity<?> create (@RequestBody Ave ave){
		return ResponseEntity.status(HttpStatus.CREATED).body(aveServicio.save(ave));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id")Long id){
		Optional<Ave> oAve = aveServicio.findById(id);
		
		if(!oAve.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oAve);
	}
	

	@GetMapping
	public ResponseEntity<?> readAll(){
		Iterable<Ave> oAve = aveServicio.findAll();
		return ResponseEntity.ok(oAve);
	}
	
	

	@GetMapping("/Todos")
	public ResponseEntity<?> readAllN(){
		Iterable<Ave> oAve = aveServicio.findAll();
		datos = (long) 0;

        oAve.forEach(new Consumer<Ave>() {
  
            @Override
            public void accept(Ave t)
            {
  
            	datos++;
            	System.out.println(datos);
            }
  
        });
    
		
		return ResponseEntity.ok("La cantidad es: "+ datos);
	}
	
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Ave aveDetalles, @PathVariable(value = "id")Long id){
		Optional<Ave> oave = aveServicio.findById(id);
		
		if(!oave.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oave.get().setAlas(aveDetalles.getAlas());
		oave.get().setPico(aveDetalles.getPico());
		
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(aveServicio.save(oave.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id")Long id){
		Optional<Ave> oAve = aveServicio.findById(id);
		
		if(!oAve.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		aveServicio.deleteById(oAve.get().getId());
		return ResponseEntity.ok(oAve);	
	}
}
