package com.ricardo.gruposalinas.app.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.ricardo.gruposalinas.app.entity.Felino;
import com.ricardo.gruposalinas.app.services.FelinoServicio;

@RestController
@RequestMapping ("/api/felinos")
public class FelinoControlller {
	Long datos = (long) 0;

	@Autowired
	private FelinoServicio felinoServicio;
	
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Felino felino){
		return ResponseEntity.status(HttpStatus.CREATED).body(felinoServicio.save(felino));
	}
	
	@GetMapping ("/{id}")
	public  ResponseEntity<?> read (@PathVariable (value = "id")Long id){
		Optional<Felino> oFelino = felinoServicio.findById(id);
		
		if(!oFelino.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oFelino);
	}
	
	

	@GetMapping
	public ResponseEntity<?> readAll(){
		Iterable<Felino> oFelino = felinoServicio.findAll();
		return ResponseEntity.ok(oFelino);
	}
	
	@GetMapping("/Todos")
	public ResponseEntity<?> readAllN(){
		Iterable<Felino> oFelino = felinoServicio.findAll();
		datos = (long) 0;

        oFelino.forEach(new Consumer<Felino>() {
  
            @Override
            public void accept(Felino t)
            {
  
            	datos++;
            	System.out.println(datos);
            }
  
        });
    
        return ResponseEntity.ok("La cantidad es: "+ datos);
	}
	
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Felino felinoDetalles, @PathVariable(value = "id")Long id){
		Optional<Felino> oFelino = felinoServicio.findById(id);
		
		if(!oFelino.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oFelino.get().setVidas(felinoDetalles.getVidas());
		oFelino.get().setCola(felinoDetalles.getCola());
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(felinoServicio.save(oFelino.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id")Long id){
		Optional<Felino> oFelino = felinoServicio.findById(id);
		
		if(!oFelino.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		felinoServicio.deleteById(oFelino.get().getId());
		return ResponseEntity.ok(oFelino);	
	}
	
	
	
}
