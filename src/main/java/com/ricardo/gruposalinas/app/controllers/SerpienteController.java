package com.ricardo.gruposalinas.app.controllers;

import java.util.Optional;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.ricardo.gruposalinas.app.entity.Serpiente;
import com.ricardo.gruposalinas.app.services.SerpienteServicio;

@RestController
@RequestMapping ("/api/serpientes")
public class SerpienteController {
	Long datos = (long) 0;

	
	@Autowired
	private SerpienteServicio serpienteServicio;
	
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Serpiente serpiente){
		return ResponseEntity.status(HttpStatus.CREATED).body(serpienteServicio.save(serpiente));
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value = "id ")Long id){
		Optional<Serpiente> oSerpiente = serpienteServicio.findById(id);
		if(!oSerpiente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(oSerpiente);
	}
	

	@GetMapping
	public ResponseEntity<?> readAll(){
		Iterable<Serpiente> oSerpiente = serpienteServicio.findAll();
		return ResponseEntity.ok(oSerpiente);
	}
	
	
	@GetMapping("/Todos")
	public ResponseEntity<?> readAllN(){
		Iterable<Serpiente> oSerpiente = serpienteServicio.findAll();
		datos = (long) 0;

        oSerpiente.forEach(new Consumer<Serpiente>() {
  
            @Override
            public void accept(Serpiente t)
            {
  
            	datos++;
            	System.out.println(datos);
            }
  
        });
    
        return ResponseEntity.ok("La cantidad es: "+ datos);
	}
	
	
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@RequestBody Serpiente serpienteDetalles, @PathVariable(value = "id")Long id){
		Optional<Serpiente> oSerpiente = serpienteServicio.findById(id);
		
		if(!oSerpiente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		oSerpiente.get().setVenenosa(serpienteDetalles.getVenenosa());
		oSerpiente.get().setPupilas(serpienteDetalles.getPupilas());
		
		
		
		return ResponseEntity.status(HttpStatus.CREATED).body(serpienteServicio.save(oSerpiente.get()));
	}
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id")Long id){
		Optional<Serpiente> oSerpiente = serpienteServicio.findById(id);
		
		if(!oSerpiente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		serpienteServicio.deleteById(oSerpiente.get().getId());
		return ResponseEntity.ok(oSerpiente);	
	}
}
