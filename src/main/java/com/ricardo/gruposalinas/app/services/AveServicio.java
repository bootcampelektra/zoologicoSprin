package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import com.ricardo.gruposalinas.app.entity.Ave;

@Service
public interface AveServicio {

	public Iterable<Ave> findAll();
	public Page<Ave>  findAll(Pageable pageable);
	public Optional<Ave>  findById(Long id);
	public Ave save(Ave ave);
	
	public void deleteById(Long id);
}
