package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ricardo.gruposalinas.app.entity.Felino;

@Service
public interface FelinoServicio {

	public Iterable<Felino> findAll();
	public Page<Felino> findAll(Pageable pageable);
	public Optional<Felino> findById(Long id);
	public Felino save(Felino felino);
	
	public void deleteById(Long id);
}
