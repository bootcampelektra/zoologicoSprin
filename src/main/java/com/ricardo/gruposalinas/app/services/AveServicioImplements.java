package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.ricardo.gruposalinas.app.entity.Ave;
import com.ricardo.gruposalinas.app.repository.AveRepositorio;

@Service
public class AveServicioImplements implements AveServicio {

	@Autowired
	
	private AveRepositorio aveServicio;

	@Override
	@Transactional (readOnly = true)
	public Iterable<Ave> findAll() {
		
		return aveServicio.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Page<Ave> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return aveServicio.findAll(pageable);
	}

	@Override
	@Transactional (readOnly = true)
	public Optional<Ave> findById(Long id) {
		return aveServicio.findById(id);
	}

	@Override
	@Transactional 
	public Ave save(Ave ave) {
		
		return aveServicio.save(ave);
	}

	@Override
	@Transactional 
	public void deleteById(Long id) {
		aveServicio.deleteById(id);
		
	}
}
