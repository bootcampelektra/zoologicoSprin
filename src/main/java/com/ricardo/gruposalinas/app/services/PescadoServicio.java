package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ricardo.gruposalinas.app.entity.Pescado;

@Service
public interface PescadoServicio {

	public Iterable<Pescado> findAll();
	public Page<Pescado> findAll(Pageable pageable);
	public Optional<Pescado> findById(Long id);
	public Pescado save (Pescado pescado);
	
	public void deleteById(Long id);
}
