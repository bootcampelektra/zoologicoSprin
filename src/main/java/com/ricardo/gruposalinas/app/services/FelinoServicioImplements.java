package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ricardo.gruposalinas.app.entity.Felino;
import com.ricardo.gruposalinas.app.repository.FelinoRepositorio;

@Service
public class FelinoServicioImplements implements FelinoServicio {

	@Autowired

	private FelinoRepositorio felinoServicio;
	
	
	@Override
	@Transactional (readOnly = true)
	public Iterable<Felino> findAll() {
		// TODO Auto-generated method stub
		return felinoServicio.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Page<Felino> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return felinoServicio.findAll(pageable);
	}

	@Override
	@Transactional (readOnly = true)
	public Optional<Felino> findById(Long id) {
		// TODO Auto-generated method stub
		return felinoServicio.findById(id);
	}

	@Override
	@Transactional 
	public Felino save(Felino felino) {
		// TODO Auto-generated method stub
		return felinoServicio.save(felino);
	}

	@Override
	@Transactional 
	public void deleteById(Long id) {
        felinoServicio.deleteById(id);
		
	}

}
