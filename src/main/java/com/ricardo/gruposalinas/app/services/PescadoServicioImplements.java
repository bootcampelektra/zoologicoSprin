package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ricardo.gruposalinas.app.entity.Pescado;
import com.ricardo.gruposalinas.app.repository.PescadoRepositorio;

@Service
public class PescadoServicioImplements implements PescadoServicio {

	@Autowired
	private PescadoRepositorio pescadoRepositorio;
	
	@Override
	@Transactional (readOnly = true)
	public Iterable<Pescado> findAll() {
		
		return pescadoRepositorio.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Page<Pescado> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return pescadoRepositorio.findAll(pageable);
	}

	@Override
	@Transactional (readOnly = true)
	public Optional<Pescado> findById(Long id) {
		// TODO Auto-generated method stub
		return pescadoRepositorio.findById(id);
	}

	@Override
	@Transactional 
	public Pescado save(Pescado pescado) {
		// TODO Auto-generated method stub
		return pescadoRepositorio.save(pescado);
	}

	@Override
	@Transactional 
	public void deleteById(Long id) {
		pescadoRepositorio.deleteById(id);
		
	}

}
