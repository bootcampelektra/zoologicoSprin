package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ricardo.gruposalinas.app.entity.Animal;
import com.ricardo.gruposalinas.app.repository.AnimalRepositorio;

@Service
public class AnimalServicioImplements implements AnimalServicio {

	@Autowired
	private AnimalRepositorio animalRepositorio;
	
	@Override
	@Transactional (readOnly = true)
	public Iterable<Animal> findAll() {
		// TODO Auto-generated method stub
		return animalRepositorio.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Page<Animal> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return animalRepositorio.findAll(pageable);
	}

	@Override
	@Transactional (readOnly = true)
	public Optional<Animal> findById(Long id) {
		// TODO Auto-generated method stub
		return animalRepositorio.findById(id);
	}

	@Override
	@Transactional
	public Animal save(Animal animal) {
		
		return animalRepositorio.save(animal);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		animalRepositorio.deleteById(id);
		
	}

}
