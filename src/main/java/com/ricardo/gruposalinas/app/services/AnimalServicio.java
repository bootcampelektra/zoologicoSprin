package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ricardo.gruposalinas.app.entity.Animal;

@Service
public interface AnimalServicio {

	public Iterable<Animal> findAll();
	public Page<Animal> findAll(Pageable pageable);
	public Optional<Animal> findById(Long id);
	public Animal save(Animal animal);
	
	public void deleteById(Long id);
}
