package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ricardo.gruposalinas.app.entity.Serpiente;

@Service
public interface SerpienteServicio {

	public Iterable<Serpiente> findAll();
	public Page<Serpiente> findAll(Pageable pageable);
	public Optional<Serpiente> findById(Long id);
	public Serpiente save(Serpiente serpiente);
	
	public void deleteById(Long id);
}
