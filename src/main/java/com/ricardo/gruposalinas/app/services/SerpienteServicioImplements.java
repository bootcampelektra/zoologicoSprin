package com.ricardo.gruposalinas.app.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ricardo.gruposalinas.app.entity.Serpiente;
import com.ricardo.gruposalinas.app.repository.SerpienteRepositorio;

@Service
public class SerpienteServicioImplements implements SerpienteServicio {

	@Autowired
	@Lazy
	private SerpienteRepositorio serpienteServicio;

	@Override
	@Transactional (readOnly = true)
	public Iterable<Serpiente> findAll() {
	
		return serpienteServicio.findAll();
	}

	@Override
	@Transactional (readOnly = true)
	public Page<Serpiente> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return serpienteServicio.findAll(pageable);
	}

	@Override
	@Transactional (readOnly = true)
	public Optional<Serpiente> findById(Long id) {
		// TODO Auto-generated method stub
		return serpienteServicio.findById(id);
	}

	@Override
	@Transactional 
	public Serpiente save(Serpiente serpiente) {
		// TODO Auto-generated method stub
		return serpienteServicio.save(serpiente);
	}

	@Override
	@Transactional 
	public void deleteById(Long id) {
	    serpienteServicio.deleteById(id);
		
	}
	
	
}
